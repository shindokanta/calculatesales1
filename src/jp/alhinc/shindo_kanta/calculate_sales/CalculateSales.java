package jp.alhinc.shindo_kanta.calculate_sales;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		Map<String, String> storeStatus = new HashMap<>();
		Map<String,Long> totalStatus = new HashMap<String,Long>();
		BufferedReader in = null;
		BufferedWriter out = null;
		FileReader fin =null;

		if(args.length != 1 ) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		try {
			String[] fruit = new String[100];
			String line ;
			File file = new File(args[0],"branch.lst");
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			fin = new FileReader(file);
			in = new BufferedReader(fin);
			while((line = in.readLine()) != null) {
				fruit = line.split(",", 0);
				if(fruit[0] == null || fruit[1] == null ) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				if(fruit[0].matches("^[0-9]{3}")==true && fruit.length == 2) {
					storeStatus.put(fruit[0],fruit[1]);
					totalStatus.put(fruit[0],0L);
				}else {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}
		}catch (NumberFormatException e) {
			System.out.println("支店定義ファイルのフォーマットが不正です");
			return ;
		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			try {
				in.close();
				fin.close();
			}catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			}
		}



		try {
			String line ;
			List<String> list1 = new ArrayList<String>();
			File cdirectory = new File(args[0]);
			String filelist[] = cdirectory.list();
			for (int i = 0 ; i < filelist.length ; i++) {
				File check = new File(args[0],filelist[i]);
				if(filelist[i].matches("^[0-9]{8}.rcd$")==true && check.isFile() == true) {
					list1.add(filelist[i]);
				}
			}
			for(int i= 0; i<list1.size()-1; i++) {
				String result = list1.get(i).substring(0,8);
				String result1 = list1.get(i+1).substring(0,8);
				int value = Integer.parseInt(result);
				int value1 = Integer.parseInt(result1);
				if(value1 - value != 1){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			long total = 0;
			for(int l= 0; l<list1.size(); l++) {
				File cdirectory1 = new File(args[0],list1.get(l));
				fin = new FileReader(cdirectory1);
				in = new BufferedReader(fin);
				List<String> list = new ArrayList<String>();
				while((line = in.readLine()) != null) {
					list.add(line);
				}
				if(list.size() != 2){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				if(list.get(1).matches("^[0-9]*$")==false){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long initial = Long.parseLong(list.get(1));
				total += initial;
				if(total>999999999){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				if(totalStatus.containsKey(list.get(0))) {
					long calculation = totalStatus.get(list.get(0)) + initial;
					totalStatus.put(list.get(0), calculation);
				}else{
					System.out.println(list1.get(l)+"の支店コードが不正です");
					return;

				}
			}
		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			try {
				in.close();
				fin.close();
			}catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}

		try{
			File file = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(file);
			out = new BufferedWriter(fw);
			for(Map.Entry<String, String> storeTotal : storeStatus.entrySet()) {
				out.write(storeTotal.getKey() + "," + storeTotal.getValue() + "," + totalStatus.get(storeTotal.getKey()));
				out.newLine();
			}
		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally{
			try{
				if (out != null) {
					out.close();
				}
			}catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}
	}
}


